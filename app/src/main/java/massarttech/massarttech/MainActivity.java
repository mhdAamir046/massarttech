package massarttech.massarttech;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {
    GoogleMap mgoogleMap;
    LocationManager locationManager;
    Double Lat, Lang;
    boolean permision = false;
    int locationCount = 0;
    Button myLocation_Btn, getDirection_Btn;
    ArrayList markerPoints = new ArrayList();
    TextView dist_TV;
    private Polyline currentPolyline;
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MapView mapView = (MapView) findViewById(R.id.mapView);
        dist_TV = findViewById(R.id.dist_TV);
        myLocation_Btn = findViewById(R.id.myLocationBtn);
        getDirection_Btn = findViewById(R.id.getDirecionBtn);
        getCurrentLocation();
        checkLocationPermision();
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
        getDirection_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  drawLineBetweenMarkers(mgoogleMap, markerPoints);
                double dist = distance(markerPoints, 'K');
                String distance = "Total Distance= " + dist + " KM";
                dist_TV.setText(distance);
                dist_TV.setVisibility(View.VISIBLE);
                if(locationCount>=2) {
                    LatLng origin = (LatLng) markerPoints.get(0);
                    LatLng dest = (LatLng) markerPoints.get(1);


                    // Getting URL to the Google Directions API
                    String url = getDirectionsUrl(origin, dest);

                    DownloadTask downloadTask = new DownloadTask();

                    // Start downloading json data from Google Directions API
                    downloadTask.execute(url);
                }
            }
        });
        myLocation_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentLocation();
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.title("Your Location");
                if (Lat != null && Lang != null) {
                    LatLng currentLocationLatLang = new LatLng(Lat, Lang);
                    markerOptions.position(currentLocationLatLang);
                    mgoogleMap.animateCamera(CameraUpdateFactory.newLatLng(currentLocationLatLang));
                    // Placing a marker on the touched position
                    mgoogleMap.addMarker(markerOptions);
                }
            }
        });
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        final boolean addMarker = true;
        mgoogleMap = googleMap;
        MapsInitializer.initialize(getApplicationContext());
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if(Lat!=null && Lang!=null) {
            Log.d("LangLat", " " + Lang + " " + Lat);
            LatLng currentLocationLatLang = new LatLng(Lat, Lang);
            googleMap.addMarker(new MarkerOptions().position(currentLocationLatLang).title("Your Location"));
            CameraPosition cameraPosition = CameraPosition.builder().target(currentLocationLatLang).zoom(16).bearing(0).tilt(45).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                locationCount = locationCount + 1;
                Toast.makeText(MainActivity.this, "" + locationCount, Toast.LENGTH_SHORT).show();
                MarkerOptions markerOptions = new MarkerOptions();
                // This will be displayed on taping the marker
                String add = getAddrees(latLng);
                switch (locationCount) {
                    case 1: {
                        markerOptions.title(add + " :Starting point");
                        markerPoints.add(latLng);
                        break;
                    }
                    case 2: {
                        markerOptions.title(add + " :End point");
                        markerPoints.add(latLng);
                        break;
                    }
                    default:
//                        showDialogue()
                }
                if (addMarker) {
                    markerOptions.position(latLng);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    // Placing a marker on the touched position
                    googleMap.addMarker(markerOptions);
                }

            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        Lat = location.getLatitude();
        Lang = location.getLatitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @SuppressLint("MissingPermission")
    public void getCurrentLocation() {
        Location location;
        try {
            locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            assert locationManager != null;
            // getting GPS status
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                Toast.makeText(this, "No Network Provider", Toast.LENGTH_SHORT).show();
            } else {
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {

                    Log.d("GPS Enabled", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            Lat = location.getLatitude();
                            Lang = location.getLongitude();
                        }
                    }
                } else if (isNetworkEnabled) {
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        if (permision) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                Lat = location.getLatitude();
                                Lang = location.getLongitude();
                            }
                        }
                    }
                }
            }
            Toast.makeText(this, " "+Lat+" "+Lang, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
        }
    }

    private void checkLocationPermision() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= 23) { // Marshmallow

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            }

        } else { // already granted
            permision = true;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1) {
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permision = true;
            } else {
                permision = false;
            }
        }
    }

    private String getAddrees(LatLng latLng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String add = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            Address obj = addresses.get(0);
            add = obj.getAddressLine(0);
            // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return add;
    }

    private void drawLineBetweenMarkers(GoogleMap mgoogleMap, ArrayList markerPoints) {
        if (markerPoints.size() >= 2) {
            LatLng origin = (LatLng) markerPoints.get(0);
        LatLng dest = (LatLng) markerPoints.get(1);
        Polyline line = mgoogleMap.addPolyline(new PolylineOptions()
                .add(origin, dest)
                .width(5)
                .color(Color.RED));
     }

    }

    private double distance(ArrayList markerPoints, char unit) {
        if (markerPoints.size() >= 2) {
            LatLng origin = (LatLng) markerPoints.get(0);
            LatLng dest = (LatLng) markerPoints.get(1);
            double lat1 = origin.latitude;
            double lon1 = origin.longitude;
            double lat2 = dest.latitude;
            double lon2 = dest.longitude;
            double theta = lon1 - lon2;
            double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K') {
                dist = dist * 1.609344;
            } else if (unit == 'N') {
                dist = dist * 0.8684;
            }
            return (dist);
        }
        return 0.00;
    }

    //converts decimal degrees to radians
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    // converts radians to decimal degrees
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin =  origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest =  dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters ="origin=" + str_origin + "&destination=" + str_dest + "&" + sensor;
        Log.d("Parameters ",""+parameters);
        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+"&key=AIzaSyAfyjkDctQx_M53nSme6kXL8x8U_FXoHqg";

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(15000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setDoInput(true);
            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                Log.d("poin tsize",""+url[0]);
                data = downloadUrl(url[0]);
                Log.d("poin tsize",""+data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            Log.d("poin tsize",""+jsonData);
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            Log.d("poin tsize",""+result);
            ArrayList<LatLng> points = new ArrayList<>();
            List<HashMap<String, String>> path=new ArrayList<>();
            HashMap<String,String> point=new HashMap<>();
            PolylineOptions lineOptions = new PolylineOptions();;
            lineOptions.width(2);
            lineOptions.color(Color.RED);
            MarkerOptions markerOptions = new MarkerOptions();

                for(int i=0;i<result.size();i++){
                    // Fetching i-th route
                     path = result.get(i);
                    // Fetching all the points in i-th route
                    for(int j=0;j<path.size();j++){
                         point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);

                }
            Log.d("poin tsize",""+points.size());
            if(points.size()!=0)
            mgoogleMap.addPolyline(lineOptions);
        }
    }

}